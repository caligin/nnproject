# -*- coding: utf-8 -*-

import numpy as np

def gauss(x, mu, sigma):
    return 1 / (sigma * np.sqrt(np.pi * 2)) * \
           np.exp(-(x - mu) ** 2 / (2 * sigma ** 2))

def logistic(x):
    return 1 / (1 + np.exp(-x))

def find_nearest_idx(array, value):
    return (np.abs(array-value)).argmin()
    
def normalize_amplitude(signal):
    maxampl = np.max(np.abs(signal))
    return signal/np.float(maxampl)
