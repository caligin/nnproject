#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import functions


# %% Defining the data distribution
mu = 1.0
sigma = 0.1
Nsamples = 10000
#generate a Nsamples data vector drawn froma a normal distribution
data = np.random.normal(mu, sigma, Nsamples)

#check if the moments are correct
meanOK = abs(mu - np.mean(data)) < 0.01
print("mean test = " + str(meanOK))
#std evaluated on N-1 items -> ddof = 1
sigmaOK = abs(sigma - np.std(data, ddof=1)) < 0.01
print("variance test = " + str(sigmaOK))

#%% Plot the data histogram as well as the underlying distribution
nbins = 100
#We first compute the histogram normalizing it to a pdf for comparison purposes
hist, bins = np.histogram(data, bins=nbins, density=True)

minbin = np.min(bins)
maxbin = np.max(bins)

pdf = functions.gauss(bins, mu, sigma)

#%% Computing CDF and plotting

#scrap an integration by simply sorting points and counting
sorted_data = np.sort(data)

plt.bar(bins[:-1], hist, width=bins[1] - bins[0], color='g')
plt.step(sorted_data, np.arange(sorted_data.size)/float(Nsamples), color='r', lw=1.5)
plt.plot(bins, pdf, color='k', lw=2, ls='--')
plt.xlim(minbin, maxbin)
plt.show()

#%% Some estimations

#the threshold function should have its steepest point aligned to the data psd highest density
#see Laughlin 1981

high_density_estimation = np.median(sorted_data)
rel_err = np.abs(mu - high_density_estimation) / mu
print("Alignment error: {0:.2f}%".format(rel_err * 100))
